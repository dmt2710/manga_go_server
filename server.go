package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/dmt2710/manga_go_server/api/manga"
)

func handleRequest() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/manga", manga.MangaIndexHandler)
	myRouter.HandleFunc("/manga/{id}", manga.MangaDetailsHandler)

	log.Fatal(http.ListenAndServe(":8080", myRouter))
}

func main() {
	handleRequest()
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Lololol")
	fmt.Println("Endpoint Hit: home page")
}
