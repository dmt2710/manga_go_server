#!/bin/bash

#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
  echo "Deploying project on server ${array[i]}"

  echo "Copying new file ..."
  scp -i ~/.ssh/id_rsa server ec2-user@${array[i]}:/home/ec2-user/server_new
  
  echo "Starting new ./server ..."
  ssh ec2-user@${array[i]} "bash start_server.sh"
done

echo "Finished!"
