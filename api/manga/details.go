package manga

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/dmt2710/manga_go_server/constants"
	"gitlab.com/dmt2710/manga_go_server/rawmodels"
)

func MangaDetailsHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method is not supported", http.StatusNotFound)
		return
	}

	vars := mux.Vars(r)
	id := vars["id"]

	result, _ := fetchMangaDetails(id)

	json.NewEncoder(w).Encode(result)
}

func fetchMangaDetails(id string) (rawmodels.MangaDetails, error) {
	response, err := http.Get(fmt.Sprintf(constants.BASE_URL+"/manga/%s", id))
	if err != nil {
		log.Fatal(err)
		return rawmodels.MangaDetails{}, err
	}
	defer response.Body.Close()

	var mangaDetails rawmodels.MangaDetails
	if err := json.NewDecoder(response.Body).Decode(&mangaDetails); err != nil {
		log.Fatal(err)
		return rawmodels.MangaDetails{}, err
	}

	return mangaDetails, nil
}
