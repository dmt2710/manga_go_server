package rawmodels

// MangaIndex ...
type MangaIndex struct {
	Results []MangaIndexResult `json:"results"`
	Limit   int                `json:"limit"`
	Offset  int                `json:"offset"`
	Total   int                `json:"total"`
}

// MangaIndexResult ...
type MangaIndexResult struct {
	Result        string                  `json:"result"`
	Data          MangaIndexItem          `json:"data"`
	Relationships []MangaItemRelationship `json:"relationships"`
}

// MangaIndexItem ...
type MangaIndexItem struct {
	ID         string              `json:"id"`
	Attributes MangaItemAttributes `json:"attributes"`
}

// MangaItemAttributes ...
type MangaItemAttributes struct {
	Title struct {
		En string `json:"en"`
	} `json:"title"`
	Description struct {
		En string `json:"en"`
	} `json:"description"`
	Status        string `json:"status"`
	ContentRating string `json:"contentRating"`
	CreatedAt     string `json:"createdAt"`
	UpdatedAt     string `json:"updatedAt"`
}

// MangaItemRelationship ...
type MangaItemRelationship struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}
