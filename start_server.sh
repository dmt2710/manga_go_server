#!/bin/bash
kill -9 `cat save_pid.pid`
rm save_pid.pid
rm server

mv server_new server

nohup ./server > server.log 2>&1 &
echo $! > save_pid.pid