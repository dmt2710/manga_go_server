package rawmodels

type MangaDetails struct {
	Result        string                  `json:"result"`
	Data          MangaDetailsItem        `json:"data"`
	Relationships []MangaItemRelationship `json:"relationships"`
}

type MangaDetailsItem struct {
	ID         string              `json:"id"`
	Status     string              `json:"status"`
	Attributes MangaItemAttributes `json:"attributes"`
}
