package manga

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/dmt2710/manga_go_server/constants"
	"gitlab.com/dmt2710/manga_go_server/rawmodels"
)

func MangaIndexHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/manga" {
		http.Error(w, "404 not found!", http.StatusNotFound)
		return
	}

	if r.Method != "GET" {
		http.Error(w, "Method is not supported", http.StatusNotFound)
		return
	}

	result, _ := fetchMangas(0, 1)

	json.NewEncoder(w).Encode(result)
}

func fetchMangas(offset int, limit int) (rawmodels.MangaIndex, error) {
	response, err := http.Get(fmt.Sprintf(constants.BASE_URL+"/manga?limit=%d&offset=%d", limit, offset))
	if err != nil {
		log.Fatal(err)
		return rawmodels.MangaIndex{}, err
	}
	defer response.Body.Close()

	var mangaIndex rawmodels.MangaIndex
	if err := json.NewDecoder(response.Body).Decode(&mangaIndex); err != nil {
		log.Fatal(err)
		return rawmodels.MangaIndex{}, err
	}

	return mangaIndex, nil
}
